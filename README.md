Project name
=====================

INFO
----
* http://test.app

INSTALL local
----
```
cp ./config.env.sample config.env
yake rebuild
```

* nginx use `jwilder/nginx-proxy` approach to run locally

DEPLOY to "amsdard.io/test/test"
----
```
yake push
``` 
